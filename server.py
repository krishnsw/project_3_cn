import socket
import threading
import os

class Server:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.accept_connections()
    
    def accept_connections(self):
        ip = socket.gethostbyname(socket.gethostname())
        port = int(input('Enter desired port --> '))

        self.s.bind((ip,port))
        self.s.listen(100)

        print('Running on IP: '+ip)
        print('Running on port: '+str(port))

        while 1:
            c, addr = self.s.accept()
            print(c)
            
            threading.Thread(target=self.handle_client,args=(c,addr,)).start()

    def handle_client(self,c,addr):
        data = c.recv(1024).decode()
    
        if not os.path.exists(data):
            c.send("file-doesn't-exist".encode())

        else:
            c.send("file-exists".encode())
            print('Sending',data)
            if data != '':
                file = open(data,'rb')
                file1 = open(data, 'r')
                diff = file1.readlines()
                if(diff >=5):
			signal_incoming = 'red'
			signal_outgoing = 'green'
		

		if(diff < 5 ):
			signal_incoming = 'green'
			signal_outgoing = 'red'
		
		
		print 'out -' + signal_outgoing
		print 'in  -' + signal_incoming
		data = file.read(1024)
		while data:
			c.send(data)
			data = file.read(1024)

		c.shutdown(socket.SHUT_RDWR)
		c.close()
				

server = Server()
